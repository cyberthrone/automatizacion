import getpass
import datetime
import tempfile
import os
import psutil
import time
import threading
import pygame
from gtts import gTTS
import requests
import logging
import queue
import socket
import struct
import platform
import collections
from stem import Signal
from stem.control import Controller
import tkinter as tk
from tkinter import scrolledtext
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer
from apscheduler.schedulers.background import BackgroundScheduler

# Inicialización de pygame para reproducción de audio
pygame.mixer.init()
pygame.mixer.music.set_volume(1.0)

# Obtener el nombre del usuario actual
nombre_usuario = getpass.getuser()

# Umbral de subida de datos para alertar
UMBRAL_SUBIDA_DATOS = 10 * 1024 * 1024  # 10 MB

# Crear un diccionario para almacenar el estado de la copia de archivos
estado_copia_archivo = {"copiando_archivo": False}

# Diccionario para almacenar las direcciones IP de destino más comunes
destino_frecuentes = collections.Counter()

# Crear una instancia de BackgroundScheduler
scheduler = BackgroundScheduler()

# Constantes
RECORDAR_PAUSA_VISTA_CADA = 30 * 60  # Cada 30 minutos
MONITOREO_RED_CADA = 60  # Cada 60 segundos
RECORDAR_ESTIRAMIENTOS_CADA = 40 * 60  # Cada 40 minutos
RECORDAR_POSTURA_CADA = 45 * 60  # Cada 45 minutos
RECORDAR_MOVER_CUELLO_CADA = 60 * 60  # Cada hora
HORAS_COMER = [8, 12, 19]  # Horas para recordar comer
RECORDAR_AGUA_CADA = 60 * 60  # Cada 1 hora
RECORDAR_PARPADEAR_CADA = 30 * 60  # Cada 30 minutos
RECORDAR_HORAS_CADA = 2 * 60 * 60  # Cada 2 horas
OPTIMIZAR_RAM_CADA = 20 * 60  # Cada 20 minutos
MONITORIZAR_RECURSOS_CADA = 30  # Cada 30 segundos
RECORDAR_DESCANSOS_CADA = 90 * 60  # Cada 90 minutos
VERIFICAR_ACTUALIZACIONES_CADA = 24 * 60 * 60  # Cada 24 horas
UMBRAL_TEMPERATURA_ALTA = 95  # Umbral para la temperatura de la CPU

# Lista de directorios a monitorear
directories_to_monitor = [
    os.path.expanduser('~/Imágenes'),
    os.path.expanduser('~/Videos'),
    os.path.expanduser('~/Música'),
    os.path.expanduser('~/Documentos'),
    os.path.expanduser('~/WorkSpace')
]

# Lista blanca de directorios
lista_blanca = [
    os.path.expanduser('/home/'),
    os.path.expanduser('~Brave')
]

# Umbral de tráfico de red para alertar
UMBRAL_BYTES = 1000000  # 1 MB

logging.basicConfig(filename='network_activity.log', level=logging.INFO, format='%(asctime)s - %(message)s')

# Configurar el proxy SOCKS5 para requests
proxies = {
    'http': 'socks5h://127.0.0.1:9050',
    'https': 'socks5h://127.0.0.1:9050'
}

# Configurar el proxy globalmente para requests
requests.proxies = proxies

def hablar(mensaje):
    try:
        # Crear una instancia de gTTS
        tts = gTTS(mensaje, lang='es')
        with tempfile.NamedTemporaryFile(suffix=".mp3", delete=False) as temp_audio_file:
            temp_audio_path = temp_audio_file.name
            tts.save(temp_audio_path)
        pygame.mixer.music.load(temp_audio_path)
        pygame.mixer.music.play()
        while pygame.mixer.music.get_busy():
            pygame.time.Clock().tick(10)
        os.remove(temp_audio_path)
    except Exception as e:
        print(f"Error al hablar: {e}")

def guardar_log(mensaje):
    logging.info(mensaje)
    log_queue.put(mensaje)

# Función para registrar eventos (ejemplo simple)
def registrar_evento(mensaje):
    with open('eventos.log', 'a') as f:
        f.write(f"{time.asctime()} - {mensaje}\n")

def detectar_redireccionamiento_trafico():
    def sniffer(packet):
        ip_header = packet[0:20]
        iph = struct.unpack('!BBHHHBBH4s4s', ip_header)
        s_addr = socket.inet_ntoa(iph[8])
        d_addr = socket.inet_ntoa(iph[9])

        destino_frecuentes[d_addr] += 1
        top_destinos = destino_frecuentes.most_common(3)

        if d_addr not in [dest[0] for dest in top_destinos]:
            mensaje = f"¡Alerta! Redireccionamiento de tráfico detectado: {s_addr} -> {d_addr}"
            hablar(mensaje)
            registrar_evento(mensaje)

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
        s.bind(('0.0.0.0', 0))
        s.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
        while True:
            packet = s.recvfrom(65565)
            sniffer(packet[0])
    except Exception as e:
        guardar_log(f"Error al detectar redireccionamiento de tráfico: {e}")

def detectar_subida_datos_sin_permisos():
    try:
        net_io_counters = psutil.net_io_counters(pernic=True)

        for interfaz, stats in net_io_counters.items():
            bytes_sent = stats.bytes_sent
            if bytes_sent > UMBRAL_SUBIDA_DATOS:
                for proc in psutil.process_iter(['pid', 'name', 'username']):
                    try:
                        connections = proc.connections()
                        for conn in connections:
                            if conn.status == psutil.CONN_ESTABLISHED and conn.raddr and conn.laddr:
                                if conn.laddr.ip == interfaz:
                                    mensaje = f"¡Alerta! El proceso {proc.info['name']} (PID: {proc.info['pid']}) está subiendo datos sin permisos."
                                    hablar(mensaje)
                                    registrar_evento(mensaje)
                    except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                        continue

        time.sleep(MONITOREO_RED_CADA)
    except Exception as e:
        guardar_log(f"Error al detectar subida de datos sin permisos: {e}")

def detectar_descargas_paquetes(actualizar_alertas):
    prev_bytes_recv = None
    prev_bytes_sent = None

    def verificar_trafico():
        nonlocal prev_bytes_recv, prev_bytes_sent
        bytes_recv = psutil.net_io_counters().bytes_recv
        bytes_sent = psutil.net_io_counters().bytes_sent

        if prev_bytes_recv is not None and (bytes_recv - prev_bytes_recv) > UMBRAL_BYTES:
            proceso_responsable = identificar_proceso_responsable(bytes_sent, bytes_recv)
            mensaje = f"¡Alerta! Se ha detectado una actividad inusual en el tráfico de red. Proceso responsable: {proceso_responsable}"
            hablar(mensaje)
            registrar_evento(mensaje)
            actualizar_alertas(mensaje)

        if prev_bytes_sent is not None and (bytes_sent - prev_bytes_sent) > UMBRAL_BYTES:
            proceso_responsable = identificar_proceso_responsable(bytes_sent, bytes_recv)
            mensaje = f"¡Alerta! El dispositivo está enviando datos excesivos. Proceso responsable: {proceso_responsable}"
            hablar(mensaje)
            registrar_evento(mensaje)
            actualizar_alertas(mensaje)

        prev_bytes_recv = bytes_recv
        prev_bytes_sent = bytes_sent
        ventana.after(60000, verificar_trafico)

    verificar_trafico()

def identificar_proceso_responsable(bytes_sent, bytes_recv):
    for proc in psutil.process_iter(['pid', 'name']):
        try:
            io_counters = proc.io_counters()
            if io_counters.write_bytes > bytes_sent / 2 or io_counters.read_bytes > bytes_recv / 2:
                return f"{proc.info['name']} (PID: {proc.info['pid']})"
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            continue
    return "Desconocido"

def iniciar_ui():
    global ventana
    ventana = tk.Tk()
    ventana.title("Monitoreo de Red")

    texto_alertas = scrolledtext.ScrolledText(ventana, width=50, height=10)
    texto_alertas.pack()

    def actualizar_alertas(mensaje):
        texto_alertas.insert(tk.END, mensaje + '\n')
        texto_alertas.yview(tk.END)

    ventana.after(1000, detectar_descargas_paquetes, actualizar_alertas)
    ventana.mainloop()
iniciar_ui()


def ajustar_volumen(nuevo_volumen):
    pygame.mixer.music.set_volume(nuevo_volumen)
    hablar(f"Volumen ajustado a {nuevo_volumen * 100}%")

def recordar_comer():
    hora_actual = datetime.datetime.now().hour
    if hora_actual in HORAS_COMER:
        hablar(f"{nombre_usuario}, es hora de comer")

def recordar_postura():
    hablar(f"{nombre_usuario}, recuerda mantener una buena postura")

def recordar_descansos():
    hablar(f"{nombre_usuario}, es hora de tomar un descanso breve para caminar o estirarse")

def recordar_agua():
    if 10 <= datetime.datetime.now().hour < 18:
        hablar("Recuerda beber agua")

def recordar_horas():
    hora_actual = time.strftime("%H:%M")
    hablar(f"La hora actual es {hora_actual}")

def verificar_actualizaciones():
    try:
        completed_process = subprocess.run(["sudo", "yay", "-Syu"], capture_output=True, text=True)
        if "upgradable" in completed_process.stdout:
            hablar("Hay actualizaciones disponibles para el sistema. Por favor, considere actualizar.")
            logging.info("Actualizaciones encontradas.")
        else:
            logging.info("El sistema está actualizado.")
    except subprocess.CalledProcessError as e:
        hablar(f"Error al verificar actualizaciones: {e}")
        logging.error(f"Error al verificar actualizaciones: {e}")

def recordar_pausa_vista():
    hablar(f"{nombre_usuario}, recuerda descansar tu vista durante 20 segundos")

def recordar_estiramientos():
    hablar(f"{nombre_usuario}, recuerda realizar estiramientos durante 5 minutos")

def recordar_mover_cuello():
    hablar(f"{nombre_usuario}, recuerda mover el cuello para evitar la torticolis")

def recordar_parpadear():
    hablar(f"{nombre_usuario}, recuerda parpadear")

def optimizar_memoria():
    try:
        os.system('sudo sync; sudo echo 3 > /proc/sys/vm/drop_caches')
        hablar("Se ha optimizado la memoria y la caché.")
        logging.info("Optimización de memoria exitosa.")
    except Exception as e:
        hablar(f"Error al optimizar la memoria: {e}")
        logging.error(f"Error al optimizar la memoria: {e}")

def verificar_puertos_abiertos():
    open_ports = []
    for proc in psutil.process_iter(['pid', 'name']):
        try:
            connections = proc.connections(kind='inet')
            for conn in connections:
                if conn.status == 'LISTEN':
                    open_ports.append(conn.laddr.port)
        except psutil.AccessDenied:
            continue
    
    open_ports = list(set(open_ports))
    if len(open_ports) > 10:
        mensaje = f"¡Alerta! Se han detectado {len(open_ports)} puertos abiertos: {open_ports}"
        hablar(mensaje)
        logging.info(mensaje)
    else:
        logging.info("Número de puertos abiertos dentro del rango aceptable.")

def verificar_cookies_cache():
    malware_detected = False
    for directory in [os.path.expanduser('~/.cache'), os.path.expanduser('~/.cookies')]:
        if os.path.exists(directory):
            for root, dirs, files in os.walk(directory):
                for file in files:
                    file_path = os.path.join(root, file)
                    try:
                        with open(file_path, 'r', errors='ignore') as f:
                            content = f.read()
                            if 'malware' in content or 'spyware' in content:
                                malware_detected = True
                                mensaje = f"¡Alerta! Posible malware detectado en: {file_path}"
                                hablar(mensaje)
                                logging.info(mensaje)
                    except (IOError, UnicodeDecodeError):
                        continue

    if not malware_detected:
        logging.info("No se detectó malware en cookies y caché.")

def verificar_seguridad_tor():
    try:
        with Controller.from_port(port=9051) as controller:
            controller.authenticate()
            controller.signal(Signal.NEWNYM)
        mensaje = "Tor está funcionando y ha sido renovado correctamente."
        hablar(mensaje)
        logging.info(mensaje)
    except Exception as e:
        mensaje = f"Error al verificar Tor: {e}"
        hablar(mensaje)
        logging.error(mensaje)

def analizar_procesos_sospechosos():
    try:
        procesos_sospechosos = []
        for proc in psutil.process_iter(['pid', 'name']):
            try:
                if 'spyware' in proc.info['name'].lower():
                    procesos_sospechosos.append(proc.info)
            except (psutil.NoSuchProcess, psutil.AccessDenied):
                continue
        
        if procesos_sospechosos:
            mensaje = "Procesos sospechosos detectados:\n" + '\n'.join([f"{proc['pid']}: {proc['name']}" for proc in procesos_sospechosos])
            hablar(mensaje)
        else:
            hablar("No se detectaron procesos sospechosos.")
        return procesos_sospechosos
    except Exception as e:
        hablar(f"Error al analizar procesos: {e}")
        return []

def detectar_spyware():
    hablar("Iniciando análisis de spyware.")
    verificar_puertos_abiertos()
    analizar_procesos_sospechosos()
    verificar_cookies_cache()

detectar_spyware()

def detectar_actividad_inusual():
    while True:
        try:
            net_traffic = psutil.net_io_counters()
            if net_traffic.bytes_sent > UMBRAL_SUBIDA_DATOS:
                message = "¡Alerta! Se ha detectado una actividad inusual en el tráfico de red"
                hablar(message)
            time.sleep(1)
        except Exception as e:
            guardar_log(f"Error al detectar actividad inusual: {e}")

def kill_zombies():
    try:
        zombies = [proc for proc in psutil.process_iter(['status']) if proc.status() == psutil.STATUS_ZOMBIE]
        for zombie in zombies:
            try:
                zombie.kill()
                guardar_log(f"Proceso zombie {zombie.pid} eliminado.")
            except psutil.NoSuchProcess:
                guardar_log(f"Proceso zombie {zombie.pid} ya no existe.")
            except psutil.AccessDenied:
                guardar_log(f"Permiso denegado para eliminar el proceso zombie {zombie.pid}.")
        
        if zombies:
            guardar_log(f"Se han eliminado {len(zombies)} procesos zombies.")
        else:
            guardar_log("No se encontraron procesos zombies.")
    except Exception as e:
        guardar_log(f"Error al eliminar procesos zombies: {e}")

def agregar_programa_lista_blanca(programa):
    try:
        lista_blanca.append(programa)
        guardar_log(f"Se ha agregado {programa} a la lista blanca")
    except Exception as e:
        guardar_log(f"Error al agregar programa a la lista blanca: {e}")

class FileCopyHandler(FileSystemEventHandler):
    def __init__(self, estado_copia):
        self.estado_copia = estado_copia

    def on_created(self, event):
        if event.is_directory:
            return
        if event.event_type == 'created':
            self.estado_copia["copiando_archivo"] = True
            print("Archivo copiándose:", event.src_path)
            if os.path.exists(event.src_path):
                try:
                    os.remove(event.src_path)
                except FileNotFoundError:
                    pass
            else:
                print(f"El archivo {event.src_path} no existe.")
            threading.Timer(5 * 60, lambda: os.remove(event.src_path) if os.path.exists(event.src_path) else None).start()

def iniciar_monitoreo_archivos():
    try:
        file_observers = []
        for directory_path in directories_to_monitor:
            if os.path.exists(directory_path):
                if directory_path in lista_blanca:
                    guardar_log(f"El directorio {directory_path} está en la lista blanca. No se monitorizará.")
                    continue
                file_observer = Observer()
                file_observer.schedule(FileCopyHandler(estado_copia_archivo), path=directory_path, recursive=True)
                file_observer.start()
                file_observers.append(file_observer)
            else:
                guardar_log(f"El directorio {directory_path} no existe.")
    except Exception as e:
        guardar_log(f"Error al iniciar el monitoreo de archivos: {e}")

logging.basicConfig(
    filename='logs.log',
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')

log_queue = queue.Queue()

def monitorear_recursos_sistema():
    while True:
        try:
            cpu_percent = psutil.cpu_percent(interval=1)
            ram_percent = psutil.virtual_memory().percent
            disco_percent = psutil.disk_usage('/').percent
            temperatura_cpu = psutil.sensors_temperatures().get('coretemp', [('coretemp', 0)])[0].current

            if cpu_percent >= 90:
                guardar_log(f"¡Alerta! Uso elevado de CPU detectado: {cpu_percent}%")
                hablar("¡Alerta! Uso elevado de CPU detectado")

            if ram_percent >= 90:
                guardar_log(f"¡Alerta! Uso elevado de RAM detectado: {ram_percent}%")
                hablar("¡Alerta! Uso elevado de RAM detectado")

            if disco_percent >= 90:
                guardar_log(f"¡Alerta! Uso elevado del disco detectado: {disco_percent}%")
                hablar("¡Alerta! Uso elevado del disco detectado")

            if temperatura_cpu >= UMBRAL_TEMPERATURA_ALTA:
                guardar_log(f"¡Alerta! Alta temperatura de CPU detectada: {temperatura_cpu}°C")
                hablar("¡Alerta! Alta temperatura de CPU detectada")

            time.sleep(MONITORIZAR_RECURSOS_CADA)
        except Exception as e:
            guardar_log(f"Error al monitorear recursos del sistema: {e}")

scheduler.add_job(recordar_comer, 'interval', hours=1)
scheduler.add_job(detectar_descargas_paquetes, 'interval', seconds=MONITOREO_RED_CADA)
scheduler.add_job(recordar_postura, 'interval', seconds=RECORDAR_POSTURA_CADA)
scheduler.add_job(recordar_descansos, 'interval', seconds=RECORDAR_DESCANSOS_CADA)
scheduler.add_job(recordar_agua, 'interval', seconds=RECORDAR_AGUA_CADA)
scheduler.add_job(recordar_horas, 'interval', seconds=RECORDAR_HORAS_CADA)
scheduler.add_job(verificar_actualizaciones, 'interval', seconds=VERIFICAR_ACTUALIZACIONES_CADA)
scheduler.add_job(recordar_pausa_vista, 'interval', seconds=RECORDAR_PAUSA_VISTA_CADA)
scheduler.add_job(recordar_estiramientos, 'interval', seconds=RECORDAR_ESTIRAMIENTOS_CADA)
scheduler.add_job(recordar_mover_cuello, 'interval', seconds=RECORDAR_MOVER_CUELLO_CADA)
scheduler.add_job(recordar_parpadear, 'interval', seconds=RECORDAR_PARPADEAR_CADA)
scheduler.add_job(optimizar_memoria, 'interval', seconds=OPTIMIZAR_RAM_CADA)
scheduler.add_job(detectar_actividad_inusual, 'interval', seconds=1)
scheduler.add_job(kill_zombies, 'interval', minutes=10)
scheduler.add_job(monitorear_recursos_sistema, 'interval', seconds=MONITORIZAR_RECURSOS_CADA)
scheduler.add_job(detectar_redireccionamiento_trafico, 'interval', seconds=MONITOREO_RED_CADA)
scheduler.add_job(detectar_subida_datos_sin_permisos, 'interval', seconds=MONITOREO_RED_CADA)

scheduler.start()

iniciar_monitoreo_archivos()

monitor_recursos_thread = threading.Thread(target=monitorear_recursos_sistema)
monitor_recursos_thread.daemon = True
monitor_recursos_thread.start()

try:
    while True:
        try:
            log_message = log_queue.get(timeout=1)
            print(log_message)
        except queue.Empty:
            continue
except KeyboardInterrupt:
    for observer in file_observers:
        observer.stop()
    for observer in file_observers:
        observer.join()
    scheduler.shutdown()
    print("Monitorización finalizada.")
